package fr.tomulus.bar.dal;

import java.util.List;

import fr.tomulus.bar.bo.Beer;

/**
 * Interface pattern DAO sépcifique aux bières
 * - implémente l'interface générqique BoissonDAO
 * 
 * @author tomulus
 * @version V0
 *
 */
public interface BeerDAO extends BoissonDAO {
	
	public List<Beer> selectByMinAlcool(float minAlcool) throws DALException ;
	
	public List<Beer> selectByMaxAlcool(float maxAlcool) throws DALException ;
	
	public List<Beer> selectByStyle(String style) throws DALException ;

}
