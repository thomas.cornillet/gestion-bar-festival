package fr.tomulus.bar.dal;

import java.util.List;

import fr.tomulus.bar.bo.Boisson;

/**
 * Interface pattern DAO sépcifique aux boissons
 * - implémente l'interface générqique DAO
 * 
 * @author tomulus
 * @version V0
 *
 */
public interface BoissonDAO extends DAO<Boisson> {
	
	public List<Boisson> selectByNom(String marque) throws DALException;
	
	public List<Boisson> selectByCategorie(String categorie) throws DALException;
	
	public List<Boisson> selectLowStock(float stock) throws DALException;

}
