package fr.tomulus.bar.dal;
// TODO revoir le système de message d'erreur
/**
 * Classe exception spécifique DAL
 * 
 * @author thomas
 * @version V0
 *
 */
public class DALException extends Exception {
	/**
	 * Default serial
	 */
	private static final long serialVersionUID = 1L;

	//Constructeurs
	public DALException() {
		super();
	}
	
	public DALException(String message) {
		super(message);
	}
	
	public DALException(String message, Throwable exception) {
		super(message, exception);
	}

	@Override
	public String getMessage() {
		StringBuffer sb = new StringBuffer("Couche DAL - ");
		sb.append(super.getMessage());
		
		return sb.toString() ;
	}
	
	
}