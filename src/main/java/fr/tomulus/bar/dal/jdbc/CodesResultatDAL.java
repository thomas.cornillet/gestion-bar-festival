package fr.tomulus.bar.dal.jdbc;

/**
 * 
 * Codes associés aux erreurs provoquées par la couche DAL
 * De 10000 à 19999
 * 
 * @author thomas
 * @version V1
 *
 */
public abstract class CodesResultatDAL {
	
	/**
	 * Les erreurs non gérées (10000-10999)
	 */
	public static final int CONNECTION_DB_ERROR = 10000;
	public static final int UPDATE_EXCEPTION = 10001;

	/**
	 * Les erreurs de requêtes SQL
	 */
	public static final int SELECT_BY_ID_NO_ID_FOUND = 11000;
	public static final int SELEC_ALL_NO_ITEM_SELECTED = 11001;
	public static final int NO_CATEGORIE = 11002;

	

}
