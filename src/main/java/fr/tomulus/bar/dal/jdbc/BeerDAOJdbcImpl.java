package fr.tomulus.bar.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.tomulus.bar.bo.Beer;
import fr.tomulus.bar.dal.BeerDAO;
import fr.tomulus.bar.dal.DALException;

/**
 * Classe d'accès aux données pour les bières
 * - utilise SQL Server
 * 
 * @author tomulus
 * @version V0
 *
 */
public class BeerDAOJdbcImpl extends BoissonDAOJdbcImpl implements BeerDAO  {
	private static final String SQL_SELECT_BY_MIN_ALCOOL = "SELECT * FROM Boissons WHERE alcool<=?";
	private static final String SQL_SELECT_BY_MAX_ALCOOL = "SELECT * FROM Boissons WHERE alcool>=?";
	private static final String SQL_SELECT_BY_STYLE = "SELECT * FROM Boissons WHERE style=?";

	@Override
	public List<Beer> selectByMinAlcool(float minAlcool) throws DALException {
		List<Beer> retour = new ArrayList<>();
		try (Connection con = JdbcTools.getConnection(); PreparedStatement pstmt = con.prepareStatement(SQL_SELECT_BY_MIN_ALCOOL)) {
			pstmt.setFloat(1, minAlcool);
			try (ResultSet rs = pstmt.executeQuery()) {
				while(rs.next()) {
					Beer beer = (Beer) testCategorie(rs);
					retour.add(beer);
				}
			}
		} catch (SQLException e) {
			throw new DALException("Erreur dans la sélection par catégorie", e);
		}
		return retour;
	}

	@Override
	public List<Beer> selectByMaxAlcool(float maxAlcool) throws DALException {
		List<Beer> retour = new ArrayList<>();
		try (Connection con = JdbcTools.getConnection(); PreparedStatement pstmt = con.prepareStatement(SQL_SELECT_BY_MAX_ALCOOL)) {
			pstmt.setFloat(1, maxAlcool);
			try (ResultSet rs = pstmt.executeQuery()) {
				while(rs.next()) {
					Beer beer = (Beer) testCategorie(rs);
					retour.add(beer);
				}
			}
		} catch (SQLException e) {
			throw new DALException("Erreur dans la sélection par catégorie", e);
		}
		return retour;
	}

	@Override
	public List<Beer> selectByStyle(String style) throws DALException {
		List<Beer> retour = new ArrayList<>();
		try (Connection con = JdbcTools.getConnection(); PreparedStatement pstmt = con.prepareStatement(SQL_SELECT_BY_STYLE)) {
			pstmt.setString(1, style);
			try (ResultSet rs = pstmt.executeQuery()) {
				while(rs.next()) {
					Beer beer = (Beer) testCategorie(rs);
					retour.add(beer);
				}
			}
		} catch (SQLException e) {
			throw new DALException("Erreur dans la sélection par catégorie", e);
		}
		return retour;
	}

}
