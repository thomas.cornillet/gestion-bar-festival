package fr.tomulus.bar.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import fr.tomulus.bar.bo.Beer;
import fr.tomulus.bar.bo.Boisson;
import fr.tomulus.bar.bo.Soft;
import fr.tomulus.bar.dal.BoissonDAO;
import fr.tomulus.bar.dal.DALException;
import fr.tomulus.bar.exceptions.BusinessException;

/**
 * Classe d'accès aux données pour les boissons
 * - utilise mariadb
 * 
 * TODO revoir la gestion des erreurs
 * TODO vérifier inject sql
 * TODO tester les procédures stockées
 * TODO revoir la gestion des code de catégorie en sontante, peut-être avec une enum
 * 
 * @author tomulus
 * @version V1
 *
 */
public class BoissonDAOJdbcImpl implements BoissonDAO {
	private static final int CATEGORIE_BEER = 2; // TODO vraiment utile ? à revoir surement vu qu'il y a une table type maintenant
	private static final int CATEGORIE_SOFT = 1; // TODO idem
	private static final String SQL_SELECT_BY_ID = "SELECT * FROM Boissons WHERE idBoisson = ?";
	private static final String SQL_SELECT_ALL = "SELECT * FROM Boissons ORDER BY nom ASC";
	private static final String SQL_UPDATE = "UPDATE Boissons SET nom=?,idTarif=?,stock=?,alcool=?,idStyle=?,gaz=?,idType=?,pichet=? WHERE idBoisson=?"; // TODO revoir avec la BLL et repenser les règles métier
	private static final String SQL_INSERT = "INSERT INTO Boissons (nom,idTarif,stock,alcool,idStyle,gaz,idType,pichet,idCategorie) VALUES (?,?,?,?,?,?,?,?,?)";
	private static final String SQL_DELETE = "DELETE Boissons WHERE idBoisson=?";
	private static final String SQL_SELECT_BY_NOM = "SELECT * FROM Boissons WHERE nom=?";
	private static final String SQL_SELECT_BY_CATEGORIE = "SELECT * FROM Boissons WHERE idCategorie=? ORDER BY nom ASC";
	private static final String SQL_SELECT_BY_LOW_STOCK = "SELECT * FROM Boissons WHERE stock<=? ORDER BY stock ASC";
	

	@Override
	public Boisson selectById(int idBoisson) throws BusinessException {
		Boisson retour = null;
		try (Connection cnx = JdbcTools.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SQL_SELECT_BY_ID);
			pstmt.setInt(1, idBoisson);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				retour = creerBoisson(rs);
			} else {
				BusinessException be = new BusinessException();
				be.ajouterErreur(CodesResultatDAL.SELECT_BY_ID_NO_ID_FOUND);
				throw be;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesResultatDAL.CONNECTION_DB_ERROR);
			throw be;
		}
		return retour;
	}
	
	@Override
	public List<Boisson> selectAll() throws BusinessException{
		List<Boisson> retour = new ArrayList<>();
		try (Connection cnx = JdbcTools.getConnection()) {
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SQL_SELECT_ALL);
			while (rs.next()) {
				Boisson boisson = creerBoisson(rs);
				retour.add(boisson);
			}
			if (retour.isEmpty()) {
				BusinessException be = new BusinessException();
				be.ajouterErreur(CodesResultatDAL.SELEC_ALL_NO_ITEM_SELECTED);
				throw be;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesResultatDAL.CONNECTION_DB_ERROR);
			throw be;
		}
		return retour;
		
	}
	
	@Override
	public void update(Boisson boisson) throws BusinessException {
		try (Connection cnx = JdbcTools.getConnection()) {
			try {
				cnx.setAutoCommit(false);
				PreparedStatement pstmt = cnx.prepareStatement(SQL_UPDATE);
				setParamPreparedStatement(pstmt, boisson);
				pstmt.setInt(8, boisson.getIdBoisson());
				pstmt.executeUpdate();
				cnx.commit();
			} catch (Exception e) {
				cnx.rollback();
				BusinessException be = new BusinessException();
				be.ajouterErreur(CodesResultatDAL.UPDATE_EXCEPTION);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesResultatDAL.CONNECTION_DB_ERROR);
			throw be;
		}
	}
	
	@Override
	public void insert(Boisson boisson){
		try (Connection cnx = JdbcTools.getConnection()) {
			try {
				cnx.setAutoCommit(false);
				PreparedStatement pstmt = cnx.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
				if (boisson instanceof Beer) {
					pstmt.setString(8, "beer");
				} else if (boisson instanceof Soft) {
					pstmt.setString(8, "soft");
				} else {
					// TODO voir quelle erreur ici
				}
				ResultSet rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					boisson.setIdBoisson(rs.getInt(1));
				} else {
					// TODO gérer le possible problème
				}
				cnx.commit();
			} catch (Exception e) {
				// changer le type d'exceptuibn ici noramlement ce sera une business exception
				cnx.rollback();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void delete(int idBoisson){
		// TODO besoin d'une transaction ?
		try (Connection cnx = JdbcTools.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SQL_DELETE);
			pstmt.setInt(1, idBoisson);
			pstmt.executeUpdate();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	@Override
	public List<Boisson> selectByNom(String nom) {
		List<Boisson> retour = new ArrayList<>();
		try (Connection cnx = JdbcTools.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SQL_SELECT_BY_NOM);
			pstmt.setString(1, nom);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Boisson boisson = creerBoisson(rs);
				retour.add(boisson);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return retour;
	}
	
	@Override
	public List<Boisson> selectByCategorie(String categorie){
		List<Boisson> retour = new ArrayList<>();
		try (Connection cnx = JdbcTools.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SQL_SELECT_BY_CATEGORIE);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Boisson boisson = creerBoisson(rs);
				retour.add(boisson);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return retour;
	}
	
	@Override
	public List<Boisson> selectLowStock(float stock) {
		List<Boisson> retour = new ArrayList<>();
		try (Connection cnx = JdbcTools.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SQL_SELECT_BY_LOW_STOCK);
			pstmt.setFloat(1, stock);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Boisson boisson = creerBoisson(rs);
				retour.add(boisson);
			}
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return retour;
	}
	
	private Boisson creerBoisson(ResultSet rs) throws SQLException {
		Boisson retour = new Boisson();
		retour.setIdBoisson(rs.getInt("idBoisson"));
		retour.setNom(rs.getString("nom_boisson"));
		retour.setIdTarif(rs.getInt("idTarif"));
		retour.setStock(rs.getFloat("stock"));
		retour.setPichet(rs.getBoolean("pichet"));
		retour.setIdCategorie(rs.getInt("idCategorie"));
		if (CATEGORIE_BEER == rs.getInt("idCategorie")) {
			((Beer) retour).setAlcool(rs.getFloat("alcool"));
			((Beer) retour).setIdStyle("idStyle");
		} else if (CATEGORIE_SOFT == rs.getInt("idCategorie")) {
			((Soft) retour).setGaz(rs.getBoolean("gaz"));
			((Soft) retour).setIdType(rs.getInt("idType"));
		} else {
			BusinessException be = new BusinessException();
			be.ajouterErreur(CodesResultatDAL.NO_CATEGORIE);
		}
		return retour;
	}

	/**
	 * Mise en place des paramètres pour les PreparedStatement
	 *
	 * 
	 * @param pstmt
	 * @param article
	 * @throws SQLException
	 */
	private void setParamPreparedStatement(PreparedStatement pstmt, Boisson boisson){
		try {
			// ,,,alcool,idStyle,gaz,idType,pichet,idCategorie
			pstmt.setString(1, boisson.getNom());
			pstmt.setInt(2, boisson.getIdTarif());
			pstmt.setFloat(3, boisson.getStock());
			if (boisson instanceof Beer) {
				pstmt.setFloat(4, ((Beer) boisson).getAlcool());
				pstmt.setString(5, ((Beer) boisson).getIdStyle());
				pstmt.setNull(6, Types.BIT);
				pstmt.setNull(7, Types.VARCHAR);
			}
			else if (boisson instanceof Soft) {
				pstmt.setNull(4, Types.FLOAT);
				pstmt.setNull(5, Types.VARCHAR);
				pstmt.setBoolean(6, ((Soft) boisson).getGaz());
				pstmt.setInt(7, ((Soft) boisson).getIdType());
			}
		} catch (SQLException e) {
			// TODO erreur lors de la récuppération des informations
			e.printStackTrace();
		}
	}
}
