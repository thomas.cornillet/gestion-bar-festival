package fr.tomulus.bar.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.tomulus.bar.bo.Soft;
import fr.tomulus.bar.dal.DALException;
import fr.tomulus.bar.dal.SoftDAO;

/**
 * Classe d'accès aux données pour les soft
 * - utilise SQL Server
 * 
 * @author tomulus
 * @version V0
 *
 */
public class SoftDAOJdbcImpl extends BoissonDAOJdbcImpl implements SoftDAO  {
	private static final String SQL_SELECT_GAZ = "SELECT * FROM Boissons WHERE gaz=?";
	private static final String SQL_SELECT_TYPE = "SELECT * FROM Boissons WHERE type=?";

	@Override
	public List<Soft> selectByGaz(boolean gazeux) throws DALException {
		List<Soft> retour = new ArrayList<>();
		try (Connection con = JdbcTools.getConnection(); PreparedStatement pstmt = con.prepareStatement(SQL_SELECT_GAZ)) {
			pstmt.setBoolean(1, gazeux);
			try (ResultSet rs = pstmt.executeQuery()) {
				while(rs.next()) {
					Soft soft = (Soft) testCategorie(rs);
					retour.add(soft);
				}
			}
		} catch (SQLException e) {
			throw new DALException("Erreur dans la sélection par catégorie", e);
		}
		return retour;
	}

	@Override
	public List<Soft> selectByType(String type) throws DALException {
		List<Soft> retour = new ArrayList<>();
		try (Connection con = JdbcTools.getConnection(); PreparedStatement pstmt = con.prepareStatement(SQL_SELECT_TYPE)) {
			pstmt.setString(1, type);
			try (ResultSet rs = pstmt.executeQuery()) {
				while(rs.next()) {
					Soft soft = (Soft) testCategorie(rs);
					retour.add(soft);
				}
			}
		} catch (SQLException e) {
			throw new DALException("Erreur dans la sélection par catégorie", e);
		}
		return retour;
	}

}
