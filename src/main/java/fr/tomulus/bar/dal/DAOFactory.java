package fr.tomulus.bar.dal;

import fr.tomulus.bar.dal.jdbc.BeerDAOJdbcImpl;
import fr.tomulus.bar.dal.jdbc.BoissonDAOJdbcImpl;
import fr.tomulus.bar.dal.jdbc.SoftDAOJdbcImpl;

/**
 * Classe d'application du pattern Factory
 * 
 * @author tomulus
 * @version V0
 *
 */
public class DAOFactory {
	public static BoissonDAO getBoissonDAO() {
		return new BoissonDAOJdbcImpl();
	}
	
	public static BeerDAO getBeerDAO() {
		return new BeerDAOJdbcImpl();
	}
	
	public static SoftDAO getSoftDAO() {
		return new SoftDAOJdbcImpl();
	}

}
