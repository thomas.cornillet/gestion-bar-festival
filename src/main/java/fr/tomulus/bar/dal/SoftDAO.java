package fr.tomulus.bar.dal;

import java.util.List;

import fr.tomulus.bar.bo.Soft;

/**
 * Interface pattern DAO sépcifique aux soft
 * - implémente l'interface générqique BoissonDAO
 * 
 * @author tomulus
 * @version V0
 *
 */
public interface SoftDAO extends BoissonDAO {
	
	public List<Soft> selectByGaz(boolean gazeux) throws DALException;
	
	public List<Soft> selectByType(String type) throws DALException;

}
