package fr.tomulus.bar.dal;

import java.util.List;

import fr.tomulus.bar.exceptions.BusinessException;

/**
 * 
 * Interface générique pour le pattern DAO
 * 
 * @author tomulus
 * @param <T>
 * @version V0
 * 
 */
public interface DAO<T> {

	public T selectById(int id) throws BusinessException;
	
	public List<T> selectAll() throws BusinessException;
	
	public void update(T data) throws BusinessException;
	
	public void insert(T data) throws DALException;
	
	public void delete(int id) throws DALException;
}
