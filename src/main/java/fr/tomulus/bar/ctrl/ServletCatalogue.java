package fr.tomulus.bar.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.tomulus.bar.bll.BLLException;
import fr.tomulus.bar.bll.CatalogueManager;
import fr.tomulus.bar.bo.Boisson;

/**
 * Servlet implementation class ServletCatalogue
 */
@WebServlet(urlPatterns = {"/catalogue", "/supprimer"})
public class ServletCatalogue extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CatalogueManager catalogueMngr = CatalogueManager.getInstance();
		if (request.getServletPath().equals("/supprimer")) {
			try {
				catalogueMngr.removeBoisson(Integer.parseInt(request.getParameter("id")));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BLLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		List<Boisson> catalogue = new ArrayList<>();
		try {
			catalogue = catalogueMngr.getCatalogue();
		} catch (BLLException e) {
			// TODO evoir la gestion des erreurs
			e.printStackTrace();
		}
		request.setAttribute("catalogue", catalogue);
		RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/jsp/catalogue.jsp");
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
