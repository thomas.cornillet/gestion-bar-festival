package fr.tomulus.bar.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.tomulus.bar.bll.BLLException;
import fr.tomulus.bar.bll.CatalogueManager;
import fr.tomulus.bar.bo.Boisson;

/**
 * Servlet implementation class ServletArticle
 */
@WebServlet(urlPatterns = {"/ajouter", "/modifier"})
public class ServletArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CatalogueManager catalogueMngr = CatalogueManager.getInstance();
		if (request.getServletPath().equals("/modifier")) {
			try {
				Boisson boisson = catalogueMngr.getBoisson(Integer.parseInt(request.getParameter("id"))); // TODO pourquoi cannot instatiante Boisson si je fai un new Boisson() plus haut ?
				request.setAttribute("modification", true);
				request.setAttribute("BoissonSelectionnee", boisson);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BLLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/jsp/boisson.jsp");
			rd.forward(request, response);
		} else if (request.getServletPath().equals("/ajouter")) {
			request.setAttribute("id", "test");
			RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/jsp/boisson.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getServletPath().equals("/ajouter")) {
			
			
			
			
			RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/jsp/boisson.jsp");
			rd.forward(request, response);
		}
	}

}
