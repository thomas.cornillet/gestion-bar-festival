package fr.tomulus.bar.bo;

/**
 * Classe modélisant un Soft
 * 
 * @author tomulus
 * @version V0
 * 
 * TODO : améliorer l'affichage
 *
 */
public class Soft extends Boisson {
	private Boolean gaz;
	private int idType;
	
	public Soft() {}

	public Boolean getGaz() {
		return gaz;
	}
	public void setGaz(Boolean gaz) {
		this.gaz = gaz;
	}
	public int getIdType() {
		return idType;
	}
	public void setIdType(int type) {
		this.idType = type;
	}

	@Override
	public String toString() {
		return String.format("[Soft] %s [gaz=%s, type=%d]", super.toString(), this.gaz?"gazeux":"plat", this.idType);
	}
	

}
