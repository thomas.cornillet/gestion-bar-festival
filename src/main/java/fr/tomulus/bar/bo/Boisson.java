package fr.tomulus.bar.bo;

/**
 * Classe abstraite modélisant une boisson
 * 
 * @author tomulus
 * @version V1
 *
 */
public class Boisson {
	private int idBoisson;
	private String nom;
	private int idTarif;
	private Float stock;
	private boolean pichet;
	private int idCategorie;
	
	public Boisson() {}

	public int getIdBoisson() {
		return idBoisson;
	}
	public void setIdBoisson(int idBoisson) {
		this.idBoisson = idBoisson;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getIdTarif() {
		return idTarif;
	}
	public void setIdTarif(int idTarif) {
		this.idTarif = idTarif;
	}
	public Float getStock() {
		return stock;
	}
	public void setStock(Float stock) {
		this.stock = stock;
	}
	public boolean isPichet() {
		return pichet;
	}
	public void setPichet(boolean pichet) {
		this.pichet = pichet;
	}
	public int getIdCategorie() {
		return idCategorie;
	}
	public void setIdCategorie(int idCategorie) {
		this.idCategorie = idCategorie;
	}

	@Override
	public String toString() {
		return "Boisson [idBoisson=" + idBoisson + ", nom=" + nom + ", idTarif=" + idTarif + ", stock=" + stock
				+ ", pichet=" + pichet + ", categorie=" + idCategorie + "]";
	};
	
}
