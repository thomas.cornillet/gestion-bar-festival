package fr.tomulus.bar.bo;

/**
 * Classe modélisant une bière
 * 
 * @author tomulus
 * @version V0
 * 
 * TODO : améliorer l'affichage
 *
 */
public class Beer extends Boisson {
	private Float alcool;
	private String idStyle;

	public Beer() {}

	public Float getAlcool() {
		return alcool;
	}
	public void setAlcool(Float alcool) {
		this.alcool = alcool;
	}
	public String getIdStyle() {
		return idStyle;
	}
	public void setIdStyle(String idStyle) {
		this.idStyle = idStyle;
	}
	
	@Override
	public String toString() {
		return String.format("[Beer] %s [gaz=%s, type=%d]", super.toString(), this.alcool, this.idStyle);
	}


}
