package fr.tomulus.bar.bll;

import java.util.List;

import fr.tomulus.bar.bo.Boisson;
import fr.tomulus.bar.bo.Beer;
import fr.tomulus.bar.bo.Soft;
import fr.tomulus.bar.dal.BoissonDAO;
import fr.tomulus.bar.dal.DALException;
import fr.tomulus.bar.dal.DAOFactory;

/**
 * Manager DAO de gestion du catalogue
 * - pattern Singleton
 * 
 * TODO gestion automatique du code tarif à partir de la clé primaire de la table code tarif
 * TODO revoir les règles d'update (pas forcément tout pouvor mettre à jour)
 * 
 * @author tomulus
 * @version V01
 *
 */
public class CatalogueManager {
	private BoissonDAO daoBoisson;
	private static CatalogueManager instance;
	
	private CatalogueManager() {
		daoBoisson = DAOFactory.getBoissonDAO();
	}
	
	public static CatalogueManager getInstance() {
		if (instance == null) {
			instance = new CatalogueManager();
		}
		return instance;
	}
	
	public void addBoisson(Boisson b) throws BLLException {
		if (b.getIdBoisson() != null) {
			throw new BLLException("La boisson est déjà enregistrée");
		}
		try {
			this.validerBoisson(b);
			this.daoBoisson.insert(b);
		} catch (DALException e) {
			throw new BLLException("Échec de l'ajout de l'article - article: "+b, e);
		}
	}
	
	public Boisson getBoisson(int index) throws BLLException {
		Boisson ret = null;
		try {
			ret = this.daoBoisson.selectById(index);
		} catch (DALException e) {
			throw new BLLException("Échec de la récupération de la boisson - idBoisson! " + index, e);
		}
		return ret;
	}
	
	public void updateBoisson(Boisson b) throws BLLException {
		try {
			this.validerBoisson(b);
			this.daoBoisson.update(b);
		} catch (DALException e) {
			throw new BLLException("Échec de la mise à jour de la boisson - boisson: " + b, e);
		}
	}
	
	public void removeBoisson(int index) throws BLLException {
		try {
			this.daoBoisson.delete(index);
		} catch (DALException e) {
			throw new BLLException("Échec de la suppression de la boisson - idBoisson: " + index, e);
		}
	}
	
	public List<Boisson> getCatalogue() throws BLLException{
		try {
			return this.daoBoisson.selectAll();
		} catch (DALException e) {
			throw new BLLException("Échec de l'obtention du catalogue", e);
		}
	}
	
	/**
	 * Vérifie les contraintes métiers sur le renseignement d'une boisson
	 * @param la boisson à vérifier
	 * @return true si valide, false si non valide
	 * @throws BLLException 
	 */
	private void validerBoisson(Boisson boisson) throws BLLException {
		Boolean valide = true;
		StringBuffer sb = new StringBuffer();
		if (boisson.getQteStock() < 0) {
			valide = false;
			sb.append("Le stock d'une boisson ne peut être négatif.\n");
		}
		try {
			List<Boisson> catalogueEnMemoire = daoBoisson.selectAll();
			for (Boisson b : catalogueEnMemoire) {
				if (b.getNom().equalsIgnoreCase(boisson.getNom())) {
					valide = false;
					sb.append("Une boisson du catalogue porte déjà le même nom.\n");
				}
			}
		} catch (DALException e) {
			valide = false;
			throw new BLLException("Erreur dans la vérification du nom de l'article.\n", e);
		}
		if (boisson.getNom().isBlank() || boisson.getNom() == null) {
			valide = false;
			sb.append("Le nom de la boisson doit être renseigné.\n");
		}
		if (boisson.getCodeTarif() == null) {
			valide = false;
			sb.append("Le code tarif de la boisson doit être renseigné.\n");
		}
		if (boisson.getCodeTarif() == 0) {
			valide = false;
			sb.append("Le code tarif de la boisson ne peut être égal à 0 (zero).\n");
		}
		if (boisson.getCodeTarif() < 0) {
			valide = false;
			sb.append("Le code tarif de la boisson ne peut être négatif.\n");
		}
		if (boisson.getQteStock() == null) {
			valide = false;
			sb.append("Le stock de la boisson doit être renseigné.\n");
		}
		if (boisson instanceof Beer) {
			if (((Beer) boisson).getAlcool() == null) {
				valide = false;
				sb.append("Le taux d'alcool d'une bière doit être renseignée.\n");
			}
			if (((Beer) boisson).getStyle().isBlank() || ((Beer) boisson).getStyle() == null) {
				valide = false;
				sb.append("Le style d'une bière doit être renseignée.\n");
			}
			if (((Beer) boisson).getStyle().length() > 30) {
				valide = false;
				sb.append("Le style d'une bière ne doit pas excéder 30 caractères.\n");
			}
		}
		if (boisson instanceof Soft) {
			if (((Soft) boisson).isGaz() == null) {
				valide = false;
				sb.append("La caractéristique gaz d'un soft doit être renseignée.\n");
			}
			if (((Soft) boisson).getType() == null) {
				valide = false;
				sb.append("Le type de soft doit être renseigné.\n");
			}
			if (((Soft) boisson).getType().length() > 30) {
				valide = false;
				sb.append("Le type d'un soft ne soit pas excéder 30 caractères.\n");
			}
		}
		if (boisson.getNom().length() > 30) {
			valide = false;
			sb.append("Le nom de la boisson ne doit pas excéder 30 caractères.\n");
		}
		if (!valide) {
			throw new BLLException(sb.toString());
		}
	}

}
