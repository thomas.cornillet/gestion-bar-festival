<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/fragments/head.jspf"></jsp:include>
<body>
	<%@ include file="/WEB-INF/fragments/header.jspf"%>
	
	<div class="container">
		<div class="row mb-4">
			<div class="col">
				<div class="jumbotron text-center">
					<h1>Bienvenue dans la gestion du bar</h1>
					<p>ça pourrait être chouette de trouver des photos de bars et de faire un carroussel</p>
				</div>
			</div>
		</div>
		
		<div class="row mb-4">
			<div class="col">
				<h2>Que souhaitez-vous faire ?</h2>
			</div>
		</div>
		
		<div class="row">
			<div class="col-12 col-lg-4">
				<div class="card mb-4">
					<div class="card-body">
						<h5 class="card-title">Afficher le catalogue</h5>
						<p class="card-text">
							Vous pourrez visionner sous forme de tableau l'intégralité du catalogue.
							Vous pourrez également éditez directement le catalogue en mettant à jour ou en supprimant des produits.
						</p>
						<a href="${pageContext.request.contextPath}/catalogue" class="btn btn-primary">Catalogue</a>
					</div>
				</div>
			</div>
			
			<div class="col-12 col-lg-4">
				<div class="card mb-4">
					<div class="card-body">
						<h5 class="card-title">Ajouter un produit</h5>
						<p class="card-text">
							Vous pourrez ajouter un nouveau produit à l'aide d'un formulaire.
						</p>
						<a href="ajouter" class="btn btn-primary">Ajout</a>
					</div>
				</div>
			</div>
			
			<div class="col-12 col-lg-4">
				<div class="card mb-4">
					<div class="card-body">
						<h5 class="card-title">Gérer les stocks</h5>
						<p class="card-text">
							Fonctionnalité à venir.
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-12 col-lg-4">
				<div class="card mb-4">
					<div class="card-body">
						<h5 class="card-title">Gérer les équipes</h5>
						<p class="card-text">
							Fonctionnalité à venir.
						</p>
					</div>
				</div>
			</div>
			
			<div class="col-12 col-lg-4">
				<div class="card mb-4">
					<div class="card-body">
						<h5 class="card-title">Afficher et exporter les ventes</h5>
						<p class="card-text">
							Fonctionnalité à venir.
						</p>
					</div>
				</div>
			</div>
		</div>
		
			
		
	</div>
	
	
	<%@ include file="/WEB-INF/fragments/footer.jspf"%>
	<%@ include file="/WEB-INF/fragments/import_js.jspf"%>
</body>
</html>