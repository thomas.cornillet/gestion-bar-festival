<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/fragments/head.jspf"></jsp:include>
<body>
	<%@ include file="/WEB-INF/fragments/header.jspf"%>
	
	<div class="container">
	  <div class="row">
	    <div class="col">
	      <h1>Article</h1>
	   	</div>
	  </div>
		<c:choose>
			<c:when test="${empty BoissonSelectionnee} && !request.getServletPath().equals('/ajouter')">
			      <div class="row">
			        <div class="col">
			          <div class="alert alert-danger alert-dismissible fade show" role="alert">
			            <h4 class="alert-heading">Erreur</h4>
			            <p>L'article n'a pas été chargé</p>
			            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			              <span aria-hidden="true">&times;</span>
			            </button>
			          </div>
			        </div>
			      </div>
			</c:when>
			<c:otherwise>
			  	<form action="${pageContext.request.contextPath}/ajouter" method="post">
           		  <div class="form-group row">
           		  	<label class="col-2 col-form-label text-right" for="nom">Nom :</label>
           		  	<!-- pour plus d'espace entre le label et l'input, ajouter l'input dans une div -->
           		  	<input type="text" class="form-control col-10" id="nom" value="${modification?BoissonSelectionnee.getNom():''}">
           		  </div>
            	</form>
			</c:otherwise>
		</c:choose>
	</div>
	
	
	<%@ include file="/WEB-INF/fragments/footer.jspf"%>
	<%@ include file="/WEB-INF/fragments/import_js.jspf"%>
</body>
</html>