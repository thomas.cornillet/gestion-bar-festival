<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/fragments/head.jspf"></jsp:include>
<body>
	<jsp:include page="/WEB-INF/fragments/header.jspf"></jsp:include>
	
	<div class="container">
	  <div class="row">
	    <div class="col">
	      <h1>Catalogue du bar</h1>
	   	</div>
	  </div>
	  <c:choose>
		<c:when test="${empty catalogue}">
	      <div class="row">
	        <div class="col">
	          <div class="alert alert-danger alert-dismissible fade show" role="alert">
	            <h4 class="alert-heading">Erreur</h4>
	            <p>Le catalogue n'a pas été chargé</p>
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">&times;</span>
	            </button>
	          </div>
	        </div>
	      </div>
		</c:when>
		<c:otherwise>
	      <div class="row">
	        <div class="col">
	          <table class="table table-striped">
				  <thead>
				    <tr>
				      <th scope="col">Catégorie</th>
				      <th scope="col">Nom</th>
				      <th scope="col">Code tarif</th>
				      <th scope="col">Stock</th>
				      <th scope="col">Style</th>
				      <th scope="col">Alcool</th>
				      <th scope="col">Type</th>
				      <th scope="col">Gaz</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<c:forEach var="article" items="${catalogue}">
	          		  <tr>
	          		  	<td>${article.getCategorie()}</td>
	          		  	<td>${article.getNom()}</td>
	          		  	<td>${article.getCodeTarif()}</td>
	          		  	<td>${article.getQteStock()}</td>
	          		  	<c:choose>
	          		  	  <c:when test="${article.getCategorie().equals(\"beer\") }">
	          		  	  	<td>${article.getStyle()}</td>
	          		  	  	<td>${article.getAlcool()}</td>
	          		  	  	<td></td>
	          		  	  	<td></td>
	          		  	  </c:when>
	          		  	  <c:otherwise>
	          		  	  	<td></td>
	          		  	  	<td></td>
	          		  	  	<td>${article.getType()}</td>
	          		  	  	<td>${article.isGaz()?"gazeux":"plat"}</td>
	          		  	  </c:otherwise>
	          		  	</c:choose>
	          		  	<td><a href="${pageContext.request.contextPath}/modifier?id=${article.getIdBoisson()}">modifier</a></td>
	          		  	<td><a href="${pageContext.request.contextPath}/supprimer?id=${article.getIdBoisson()}">supprimer</a></td>
	          		  </tr>
	         		</c:forEach>
			  	  </tbody>
			  </table>
	        </div>
	      </div>
		</c:otherwise>
	  </c:choose>
	</div>
	

	<%@ include file="/WEB-INF/fragments/footer.jspf"%>
	<%@ include file="/WEB-INF/fragments/import_js.jspf"%>
</body>

</html>